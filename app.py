from flask import Flask, request, render_template
from sklearn import metrics 
import pandas as pd
import numpy as np
import pickle

from feature import FeatureExtraction
import warnings
warnings.filterwarnings('ignore')

# declare constants
HOST = '0.0.0.0'
PORT = 8081

# initialize flask application
app = Flask(__name__)
model = pickle.load(open('modelfold/modelRF.pkl', 'rb'))

'''@app.route("/")
def home():
    return render_template("index.html")'''

@app.route("/", methods=["GET", "POST"])
def predict():
    
    if request.method == "POST":

        input_url = request.form["vurl"]
        obj = FeatureExtraction(input_url)
        url = np.array(obj.getFeaturesList()).reshape((1, -1)) 

        pred = model.predict(url)[0]

        if (pred) == 0:
            res="SAFE"
            prob = model.predict_proba(url)[0,0]
            prob_t = "{0:.2f} % ".format(prob*100)
            return render_template("index.html", prediction_text = res)
            #return res , prob_t

        elif (pred) == 1:
            res="DEFACEMENT"
            prob = model.predict_proba(url)[0,1]
            prob_t = "{0:.2f} % ".format(prob*100)
            return render_template("index.html", prediction_text = res , percentage = prob_t)
            #return res , prob_t

        elif (pred) == 2:
            res="MALWARE"
            prob = model.predict_proba(url)[0,2]
            prob_t = "{0:.2f} % ".format(prob*100)
            return render_template("index.html", prediction_text = res , percentage = prob_t)
            #return res , prob_t
            
        elif (pred) == 3:
            res="PHISHING"
            prob = model.predict_proba(url)[0,3]
            prob_t = "{0:.2f} % ".format(prob*100)
            return render_template("index.html", prediction_text = res , percentage = prob_t)
            # return res , prob_t


        #return render_template("index.html", prediction_text = res , percentage = prob_t)
    
    return render_template("index.html")

if __name__ == '__main__':
    # run web server
    app.run(host=HOST,
            debug=True,  # automatic reloading enabled
            port=PORT)


